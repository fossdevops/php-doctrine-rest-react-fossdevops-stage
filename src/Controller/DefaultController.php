<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Swagger\Annotations as SWG;

class DefaultController extends AbstractController
{
    /**
     * @SWG\Get(
     *     path="/api/hasdb",
     *     description="Get the current database connectivity state",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Returns the current db connectivity state", 
     *         @SWG\Schema(
     *             type="object"
     *     ))
     * )
     * @Route(
     *     "/api/hasdb",
     *     methods={"GET"},
     *     name="has_db")
     */
    public function hasdb()
    {
        //$number = random_int(0, 100);
        $connected = false;
        try{
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->connect();
            $connected = $em->getConnection()->isConnected();
        } catch(\Exception $e){
            //pass
        }

        return new Response(
            json_encode(['db_connected' => ($connected ? true : false)])
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/version",
     *     description="Get the currently deployed app version",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response="200",
     *         description="Returns version of the app", 
     *         @SWG\Schema(
     *             type="object"
     *     ))
     * )
     * @Route(
     *     "/api/version",
     *     methods={"GET"},
     *     name="version")
     */
    public function version()
    {
        return new Response(
            json_encode(['version'=>trim(\file_get_contents(dirname(dirname(__DIR__)).'/VERSION'))])
        );
    }
}