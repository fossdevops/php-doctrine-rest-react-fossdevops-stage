$(document).ready(function () {
    $("input[type=radio][name=figure]").change(function() {
        var radioBtnVal = this.value;
        $.ajax({

            url : 'index.php?figure='+radioBtnVal,
            type : 'POST',
            data : {
            },
            dataType:'json',
            success : function(data) {              
                //alert('Data: '+JSON.stringify(data));
                if (!data.hasOwnProperty('error') || data['error'] === null){
                    if(!data.hasOwnProperty('data') || data['data'] === null){
                        $('#answer').text('ERROR: data value was null, but no error was set!');
                    }else{
                        if(!data['data'].hasOwnProperty('answer') || data['data']['answer'] === null){
                            $('#answer').text('ERROR: answer value was null, but no error was set!');
                        }else{
                            var obj = $('#answer');
                            obj.html("<pre>Answer count: "+data['data']['answer'].length+"\n"+JSON.stringify(data['data']['answer'],null, 2)+"</pre>");
                        }
                    }
                }
                else{
                    $('#answer').text("err: "+data['error']);
                }
                if (data.hasOwnProperty('trace') && data['trace'] !== null){
                    $('#trace').html("<pre>"+data['trace']+"</pre>");
                }
            },
            error : function(request, error)
            {
                alert("Request: "+JSON.stringify(request)+"\nError: "+error);
            },
            beforeSend: function() {
                $('#answer').html("");
                $('#trace').html("");
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
        });
    });
});