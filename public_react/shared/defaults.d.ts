import type { SSH, Server } from './interfaces';
export declare const sshDefault: SSH;
export declare const serverDefault: Server;
export declare const forceSSHDefault: boolean;
export declare const defaultCommand: string;
export declare const defaultLogLevel: string;
