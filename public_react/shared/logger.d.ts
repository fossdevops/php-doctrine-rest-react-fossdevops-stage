import winston from 'winston';
export declare function setLevel(level: typeof winston.level): void;
export declare function logger(): winston.Logger;
