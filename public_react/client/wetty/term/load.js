import _ from "../../../web_modules/pkg/lodash.js";
export const defaultOptions = {
  xterm: {fontSize: 14},
  wettyVoid: 0,
  wettyFitTerminal: true
};
export function loadOptions() {
  try {
    let options = _.isUndefined(localStorage.options) ? defaultOptions : JSON.parse(localStorage.options);
    if (!("xterm" in options)) {
      const xterm = options;
      options = defaultOptions;
      options.xterm = xterm;
    }
    return options;
  } catch {
    return defaultOptions;
  }
}
