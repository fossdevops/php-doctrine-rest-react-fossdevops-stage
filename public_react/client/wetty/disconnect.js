import _ from "../../web_modules/pkg/lodash.js";
import {verifyPrompt} from "./disconnect/verify.js";
import {overlay} from "./disconnect/elements.js";
export function disconnect(reason) {
  if (_.isNull(overlay))
    return;
  overlay.style.display = "block";
  const msg = document.getElementById("msg");
  if (!_.isUndefined(reason) && !_.isNull(msg))
    msg.innerHTML = reason;
  window.removeEventListener("beforeunload", verifyPrompt, false);
}
