import _ from "../web_modules/pkg/lodash.js";
import {dom, library} from "../web_modules/pkg/@fortawesome/fontawesome-svg-core.js";
import {faCogs} from "../web_modules/pkg/@fortawesome/free-solid-svg-icons.js";
import {FileDownloader} from "./wetty/download.js";
import {disconnect} from "./wetty/disconnect.js";
import {mobileKeyboard} from "./wetty/mobile.js";
import {overlay} from "./wetty/disconnect/elements.js";
import {socket} from "./wetty/socket.js";
import {verifyPrompt} from "./wetty/disconnect/verify.js";
import {terminal} from "./wetty/term.js";
import {FlowControlClient} from "./wetty/flowcontrol.js";
library.add(faCogs);
dom.watch();
function onResize(term) {
  return function resize() {
    term.resizeTerm();
  };
}
socket.on("connect", () => {
  const term = terminal(socket);
  if (_.isUndefined(term))
    return;
  if (!_.isNull(overlay))
    overlay.style.display = "none";
  window.addEventListener("beforeunload", verifyPrompt, false);
  window.addEventListener("resize", onResize(term), false);
  term.resizeTerm();
  term.focus();
  mobileKeyboard();
  const fileDownloader = new FileDownloader();
  const fcClient = new FlowControlClient();
  term.onData((data) => {
    socket.emit("input", data);
  });
  term.onResize((size) => {
    socket.emit("resize", size);
  });
  socket.on("data", (data) => {
    const remainingData = fileDownloader.buffer(data);
    /*const downloadLength = data.length - remainingData.length;
    if (downloadLength && fcClient.needsCommit(downloadLength)) {
      socket.emit("commit", fcClient.ackBytes);
    }
    if (remainingData) {
      if (fcClient.needsCommit(remainingData.length)) {
        term.write(remainingData, () => socket.emit("commit", fcClient.ackBytes));
      } else {
        term.write(remainingData);
      }
    }*/
    if (!__WETTY_EVAL_OUTPUT__) {
      term.write(remainingData)
    } else {
      const challengeResults = __WETTY_EVAL_OUTPUT__(`${remainingData}`);
      if (challengeResults) {
          const { results, filteredOutput } = challengeResults;
          term.write(filteredOutput)
          if (results.hash) {
          __POST_RESULTS__(results);
          } else {
          __SEND_MSG__(results);
          }
      } else {
          term.write(remainingData)
      }
    }
  }).on("login", () => {
    term.writeln("");
    term.resizeTerm();
  }).on("logout", disconnect).on("disconnect", disconnect).on("error", (err) => {
    if (err)
      disconnect(err);
  });
});
